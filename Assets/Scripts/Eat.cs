using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat : MonoBehaviour
{
    public EatType type;
    public Sprite[] FruitImages;
    public Sprite[] VegateblesImages;
    public int scoreValue;
    public SpriteRenderer renderer;

    private void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    public int randomImage()
    {
        int count;

        if (type == EatType.Fruit)
        {
            count = Random.Range(0, FruitImages.Length);

            return count;
        }
        else if(type == EatType.Vegetables)
        {
            count = Random.Range(0, VegateblesImages.Length);

            return count;
        }

        return 0;
    }
}

public enum EatType
{
    Fruit,
    Vegetables
}



