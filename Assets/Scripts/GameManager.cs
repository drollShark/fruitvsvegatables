using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject eat;

    public Eat eatScript;

    [Range(0,20)]
    public int StartCountEat = 3;

    [Range(0, 10)]
    public int EatSpawnIteam;

    private void Start()
    {
        for (int i = 0; i < StartCountEat; i++)
        {
            Spawn();
        }
    }

    void Spawn()
    {
        Instantiate(eat, eat.transform.position, transform.rotation);

        eatScript = eat.GetComponent<Eat>();

        eatScript.type = (EatType)typeof(EatType).GetRandomEnumValue();

        if(eatScript.type == EatType.Fruit)
        {
            eatScript.renderer.sprite = eatScript.FruitImages[eatScript.randomImage()];
        }
        else if (eatScript.type == EatType.Vegetables)
        {
            eatScript.renderer.sprite = eatScript.VegateblesImages[eatScript.randomImage()];
        }

    }
}

public static class EnumExtensions
{
    public static Enum GetRandomEnumValue(this Type t)
    {
        return Enum.GetValues(t).OfType<Enum>().OrderBy(e => Guid.NewGuid()).FirstOrDefault();           
    }
}


