using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectedObj : MonoBehaviour
{
    public GameObject SpawnGameObject;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Eat"))
        {
            collision.gameObject.transform.position = SpawnGameObject.transform.position;
        }


    }
}
